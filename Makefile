CC=gcc
CFLAGS=-Wall -Wextra -pedantic -ansi -g

all: q qd

q: q.c
	$(CC) $(CFLAGS) -o q q.c
qd: qd.o list.o
	$(CC) $(CFLAGS) -o qd qd.o list.o

qd.o: qd.c list.h
	$(CC) $(CFLAGS) -c -o qd.o qd.c

list.o: list.c list.h
	$(CC) $(CFLAGS) -c -o list.o list.c

clean:
	rm -f *.o qd
