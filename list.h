#ifndef _list_h_
#define _list_h_

struct list_node {
	void *data;
	struct list_node *prev;
	struct list_node *next;
};

struct list {
	struct list_node *head;
	struct list_node *tail;
	unsigned size;
};

struct list *list_create(void);
void list_destroy(struct list *l);
void list_purge(struct list *l);

void *list_pop(struct list *l);
int list_push(struct list *l, void *data);
void *list_shift(struct list *l);
int list_unshift(struct list *l, void *data);

void *list_delete(struct list *l, int pos);

unsigned list_size(struct list *l);
int list_empty(struct list *l);

#endif
