#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SOCK_PATH "/tmp/q.socket"
#define BUFSIZE 1024

int
connect_to_daemon(void)
{
	int s, len;
	struct sockaddr_un remote;

	if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
		return -1;
	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, SOCK_PATH);
	len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	if (connect(s, (struct sockaddr *)&remote, len) == -1)
		return -1;
	return s;
}

void
print_usage(void)
{
	fputs("Usage: q <queue cmd> [<queue cmd args>]\n", stderr);
	fputs("  queue commands:\n", stderr);
	fputs("    q        quit daemon\n", stderr);
	fputs("    l        list queue\n", stderr);
	fputs("    i        queue info\n", stderr);
	fputs("    s        stop queue\n", stderr);
	fputs("    r        run queue\n", stderr);
	fputs("    8        repeat queue\n", stderr);
	fputs("    f        flush queue\n", stderr);
	fputs("    d <n>    delete n-th item in queue\n", stderr);
	fputs("    a <cmd>  add cmd to queue\n", stderr);
}

int main(int argc, char **argv)
{
	int socket, t, i;
	char str[BUFSIZE];

	/* input validation */
	/* TODO simplify */
	if (argc == 1) {
		print_usage();
		return 1;
	} else if (argc == 2) {
		if (!(strcmp(argv[1], "q") == 0
			|| strcmp(argv[1], "l") == 0
			|| strcmp(argv[1], "i") == 0
			|| strcmp(argv[1], "s") == 0
			|| strcmp(argv[1], "r") == 0
			|| strcmp(argv[1], "8") == 0
			|| strcmp(argv[1], "f") == 0)) {
			print_usage();
			return 1;
		}
	} else if (argc == 3) {
		if (!(strcmp(argv[1], "d") == 0)) {
			print_usage();
			return 1;
		}
	} else if (!(strcmp(argv[1], "a") == 0)) {
		print_usage();
		return 1;
	}

	if ((socket = connect_to_daemon()) == -1) {
		perror("q");
		return 1;
	} 

	sprintf(str, "%s", argv[1]);
	for (i = 2; i < argc; i++)
		sprintf(str, "%s %s", str, argv[i]);

	if (send(socket, str, strlen(str), 0) == -1) {
		perror("q");
		return 1;
	}
	if ((t = recv(socket, str, BUFSIZE, 0)) > 0) {
		str[t] = '\0';
		printf("%s", str);
	} else {
		if (t < 0) {
			perror("recv");
		} else {
			printf("Server closed connection\n");
		}
		return 1;
	}
	close(socket);
	return 0;
}
