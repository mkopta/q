#include <stdlib.h>
#include "list.h"

struct list *
list_create(void)
{
	struct list *l = malloc(sizeof(struct list));
	if (l == NULL)
		return NULL;
	l->tail = NULL;
	l->head = NULL;
	l->size = 0;
	return l;
}

void
list_destroy(struct list *l)
{
	list_purge(l);
	free(l);
}

void
list_purge(struct list *l)
{
	while (!list_empty(l)) {
		free(list_pop(l));
	}
}

void *
list_pop(struct list *l)
{
	void *data = NULL;
	struct list_node *n = l->tail;
	if (n->prev == NULL) {
		l->head = NULL;
		l->tail = NULL;
		l->size = 0;
	} else {
		l->tail = l->tail->prev;
		l->tail->next = NULL;
		l->size--;
	}
	data = n->data;
	free(n);
	return data;
}

int
list_push(struct list *l, void *data)
{
	struct list_node *n = malloc (sizeof(struct list_node));
	if (n == NULL)
		return 1;
	n->prev = NULL;
	n->next = NULL;
	n->data = data;
	if (l->size == 0) {
		l->head = n;
		l->tail = n;
		l->size = 1;
	} else {
		l->tail->next = n;
		n->prev = l->tail;
		l->tail = n;
		l->size++;
	}
	return 0;
}

void *
list_shift(struct list *l)
{
	void *data;
	struct list_node *n = l->head;
	if (l->head->next == NULL) {
		l->head = NULL;
		l->tail = NULL;
		l->size = 0;
	} else {
		l->head = l->head->next;
		l->head->prev = NULL;
		l->size--;
	}
	data = n->data;
	free(n);
	return data;
}

int
list_unshift(struct list *l, void *data)
{
	struct list_node *n = malloc (sizeof(struct list_node));
	if (n == NULL)
		return 1;
	n->prev = NULL;
	n->next = NULL;
	n->data = data;
	if (l->size == 0) {
		l->head = n;
		l->tail = n;
		l->size = 1;
	} else {
		n->prev = NULL;
		n->next = l->head;
		l->head->prev = n;
		l->head = n;
		l->size++;
	}
	return 0;
}

void *
list_delete(struct list *l, int pos)
{
	int i = 0;
	struct list_node *n = l->head;
	void *data;
	for (; n != NULL && i != pos; n = n->next, i++)
		;
	if (!n)
		return NULL;
	if (n->prev)
		n->prev->next = n->next;
	if (n->next)
		n->next->prev = n->prev;
	if (l->head == n)
		l->head = n->next;
	if (l->tail == n)
		l->tail = n->prev;
	data = n->data;
	free(n);
	l->size--;
	return data;
}

unsigned
list_size(struct list *l)
{
	return l->size;
}

int
list_empty(struct list *l)
{
	return l->size == 0;
}
