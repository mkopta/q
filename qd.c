#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/un.h>
#include "list.h"

#define SOCK_PATH "/tmp/q.socket"
#define BUFSIZE 1024

enum qstat {
	QSTAT_RUNNING,
	QSTAT_REPEATING,
	QSTAT_STOPPED,
	QSTAT_QUIT
};

void qlist(int, pid_t, char *, struct list *);
int qadd(char *, struct list *);
int qdel(char *, struct list *);
void qinfo(int, pid_t, char *, int, struct list *);
int unknown_command(int);
int handle_input(char *, int, pid_t, char *, int *, struct list *);
int handle_connection(int, pid_t, char *, int *, struct list *);
int create_main_socket(void);
void write_pid_to_file(char *);
void daemonize(char *);
int main(void);

void
qlist(int socket, pid_t runn_cmd_pid, char *runn_cmd, struct list *queue)
{
	int cmd_no = 0;
	struct list_node *item = queue->head;
	char buffer[BUFSIZE];

	if (runn_cmd_pid) {
		sprintf(buffer, "running: [%d] %s\n", runn_cmd_pid, runn_cmd);
		send(socket, buffer, strlen(buffer), MSG_NOSIGNAL);
	}
	for (; item != NULL; item = item->next) {
		sprintf(buffer, "%2i %s\n", cmd_no++, (char *) item->data);
		send(socket, buffer, strlen(buffer), MSG_NOSIGNAL);
	}
}

int
qadd(char *input, struct list *queue)
{
	int i = 0, j = -1;
	char *new_item = malloc((strlen(input) + 1) * sizeof(char));

	if (!new_item)
		return 1;
	while (input[++j] != ' ' && input[j])
		;
	if (!input[j])
		return 1;
	while (input[++j])
		if (input[j] != '\n')
			new_item[i++] = input[j];
	new_item[i] = '\0';
	list_push(queue, new_item);
	return 0;
}

int
qdel(char *input, struct list *queue)
{
	int i = 0, j = -1;
	char *cmd;

	while (input[i] && input[i++] != ' ')
		;
	if (!input[i])
		return 1;
	sscanf(input + i, "%d", &j);
	if (j < 0 || (unsigned) (j + 1) > list_size(queue))
		return 1;
	if ((cmd = list_delete(queue, j)))
		free(cmd);
	return 0;
}

void
qinfo(int socket, pid_t runn_cmd_pid, char *runn_cmd, int qstat,
		struct list *queue)
{
	char buffer[2 * BUFSIZE];
	if (runn_cmd_pid) {
		sprintf(buffer, "RUNNING PID %i\n", runn_cmd_pid);
		send(socket, buffer, strlen(buffer), MSG_NOSIGNAL);
		sprintf(buffer, "RUNNING CMD %s\n", runn_cmd);
		send(socket, buffer, strlen(buffer), MSG_NOSIGNAL);
	}
	switch (qstat) {
	case QSTAT_RUNNING: sprintf(buffer, "QUEUE IS RUNNING\n"); break;
	case QSTAT_STOPPED: sprintf(buffer, "QUEUE IS STOPPED\n"); break;
	case QSTAT_REPEATING: sprintf(buffer, "QUEUE IS REPEATING\n"); break;
	}
	send(socket, buffer, strlen(buffer), MSG_NOSIGNAL);
	sprintf(buffer, "QUEUE SIZE %i\n", list_size(queue));
	send(socket, buffer, strlen(buffer), MSG_NOSIGNAL);
}

int
unknown_command(int socket)
{
	send(socket, "Unknown command\n", strlen("Unknown command\n"),
			MSG_NOSIGNAL);
	return 1;
}

int
handle_input(char *input, int socket, pid_t runn_cmd_pid, char *runn_cmd,
		int *qstat, struct list *queue)
{
	if (strlen(input) == 0)
		return 0;
	switch (input[0]) {
	case 'q': *qstat = QSTAT_QUIT; break;
	case 'a': return qadd(input, queue);
	case 'd': return qdel(input, queue);
	case 'f': list_purge(queue); break;
	case 's': *qstat = QSTAT_STOPPED; break;
	case 'r': *qstat = QSTAT_RUNNING; break;
	case 'i': qinfo(socket, runn_cmd_pid, runn_cmd, *qstat, queue); break;
	case 'l': qlist(socket, runn_cmd_pid, runn_cmd, queue); break;
	case '8': *qstat = QSTAT_REPEATING; break;
	default: return unknown_command(socket);
	}
	return 0;
}

int
handle_connection(int main_socket, pid_t runn_cmd_pid, char *runn_cmd,
		int *qstat, struct list *queue)
{
	int socket, n, retval;
	struct sockaddr_un remote;
	socklen_t t = sizeof(remote);
	char buf[BUFSIZE];

	fcntl(main_socket, F_SETFL, O_NONBLOCK);
	memset(buf, '\0', BUFSIZE);
	if ((socket = accept(main_socket, (struct sockaddr *)&remote, &t))
			== -1)
		return 0;
	n = recv(socket, buf, BUFSIZE, 0);
	if (n < 0) {
		send(socket, "NOK\n", strlen("NOK\n"), MSG_NOSIGNAL);
	} else if (n > 0) {
		retval = handle_input(buf, socket, runn_cmd_pid, runn_cmd,
				qstat, queue);
		if (retval == 1) {
			send(socket, "NOK\n", strlen("NOK\n"), MSG_NOSIGNAL);
		} else {
			send(socket, "OK\n", strlen("OK\n"), MSG_NOSIGNAL);
		}
	}
	close(socket);
	return retval;
}

int create_main_socket(void)
{
	int len, main_socket;
	struct sockaddr_un local;

	if ((main_socket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
		return -1;
	local.sun_family = AF_UNIX;
	strcpy(local.sun_path, SOCK_PATH);
	unlink(local.sun_path);
	len = strlen(local.sun_path) + sizeof(local.sun_family);
	if (bind(main_socket, (struct sockaddr *)&local, len) == -1)
		return -1;
	if (listen(main_socket, 5) == -1)
		return -1;
	return main_socket;
}

void
write_pid_to_file(char *fname)
{
	FILE *fp;

	if (!(fp = fopen(fname, "w+"))
			|| fprintf(fp, "%d\n", (int) getpid()) < 0
			|| fclose(fp) == EOF)
		goto write_pid_to_file_error;
	return;
write_pid_to_file_error:
	(void) fclose(fp);
	fprintf(stderr, "Failed to write pid to file '%s'.\n", fname);
	exit(EXIT_FAILURE);
}

void
daemonize(char *pidfile)
{
	pid_t pid = fork();

	if (pid < 0) {
		fprintf(stderr, "Failed to fork().\n");
		exit(EXIT_FAILURE);
    } else if (pid > 0) {
		exit(EXIT_SUCCESS);
    }
	if (pidfile)
		write_pid_to_file(pidfile);
	umask(0);
    (void) umask(0777);
	if (setsid() < 0
			|| chdir("/") < 0
			|| close(STDIN_FILENO) < 0
			|| close(STDOUT_FILENO) < 0
			|| close(STDERR_FILENO) < 0) {
		fprintf(stderr, "Failed to daemonize.\n");
		exit(EXIT_FAILURE);
	}
}

int
main(void)
{
	int main_socket, qstat = QSTAT_RUNNING;
	pid_t pid = 0;
	char *cmd = NULL, *pidfile = NULL;
	struct list *queue = list_create();

	daemonize(pidfile);

	if ((main_socket = create_main_socket()) == -1) {
		perror("qd");
		return 1;
	}
LOOP:
	if (pid && waitpid(pid, NULL, WNOHANG) == pid) {
		waitpid(pid, NULL, 0);
		if (qstat == QSTAT_REPEATING) {
			list_push(queue, cmd);
		} else {
			free(cmd);
		}
		cmd = NULL;
		pid = 0;
	}
	handle_connection(main_socket, pid, cmd, &qstat, queue);
	if (qstat == QSTAT_QUIT) {
		if (pid)
			waitpid(pid, NULL, 0);
		return 0;
	}
	sleep(1);
	if (pid || list_empty(queue) || qstat == QSTAT_STOPPED)
		goto LOOP;
	cmd = list_shift(queue);
	if ((pid = fork()) == 0)
		return system(cmd);
	if (pid == -1) {
		pid = 0;
		list_unshift(queue, cmd);
	}
	goto LOOP;
}
